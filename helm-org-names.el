;;; helm-org-names.el --- Helm navigation of source blocks, tables and figures in Org Mode    -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Juan Manuel Macías

;; Author: Juan Manuel Macías <maciaschain@posteo.net>
;; URL: https://gitlab.com/maciaschain/org-font-spec-preview
;; Version: 0.9
;; Package-Requires: ((emacs "27.2") (org "9.4")) (helm "3.8.4")
;; Keywords: org-mode, helm, tables, figures, code blocks

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

  ;;; Code:

(provide 'helm-org-names)

(require 'helm)

(setq helm-org-names-buf nil)

;;;###autoload
(defun helm-org-names-src-candidates ()
  (org-babel-src-block-names helm-org-names-buf))

(setq helm-org-names-src-actions
      (helm-make-actions
       "Go to block" (lambda (candidate)
			(let ((point (org-babel-find-named-block candidate)))
			  (goto-char point)
			  (org-show-entry)))
       "Go to block (other buffer)" (lambda (candidate)
				     (let ((ind-buf (concat (buffer-name) "--copy")))
				       (when (get-buffer ind-buf)
					 (kill-buffer ind-buf))
				       (clone-indirect-buffer-other-window ind-buf t)
				       (let ((point (org-babel-find-named-block candidate)))
					 (goto-char point)
					 (org-show-entry)
					 (recenter 0))))
       "Edit block" (lambda (candidate)
			    (let ((point (org-babel-find-named-block candidate)))
			      (save-excursion
				(goto-char point)
				(org-edit-special))))))

(setq helm-org-names-src-sources
      '((name . "Code blocks")
	(candidates . helm-org-names-src-candidates)
	(action . helm-org-names-src-actions)))

;;;###autoload
(defun helm-org-names-table-names (&optional file)
  "Return the names of tables in FILE or the current buffer."
  (with-current-buffer (if file (find-file-noselect file) (current-buffer))
    (org-with-point-at 1
      (let ((names nil))
	(while (re-search-forward "^|" nil t)
	  (let ((element (save-excursion
			   (beginning-of-line)
			   (org-element-at-point))))
	    (when (eq 'table (org-element-type element))
	      (let ((name (org-element-property :name element)))
		(when name (push name names))))))
	names))))

;;;###autoload
(defun helm-org-names-table-candidates ()
  (helm-org-names-table-names helm-org-names-buf))

(setq helm-org-names-table-actions
      (helm-make-actions
       "Go to table" (lambda (candidate)
		       (let ((case-fold-search t)
			     (point (save-excursion
				      (goto-char (point-min))
				      (re-search-forward (concat "#\\+[tbl]*name:\s*" candidate) nil t))))
			 (goto-char point)
			 (org-show-entry)))
       "Go to table (other buffer)" (lambda (candidate)
				      (let ((ind-buf (concat (buffer-name) "--copy")))
					(when (get-buffer ind-buf)
					  (kill-buffer ind-buf))
					(clone-indirect-buffer-other-window ind-buf t)
					(let ((case-fold-search t)
					      (point (save-excursion
						       (goto-char (point-min))
						       (re-search-forward (concat "#\\+[tbl]*name:\s*" candidate) nil t))))
					  (goto-char point)
					  (org-show-entry)
					  (recenter 0))))
       "Link a table" (lambda (candidate)
			(insert (concat "[[" candidate "]]")))))

(setq helm-org-names-table-sources
      '((name . "Tables")
	(candidates . helm-org-names-table-candidates)
	(action . helm-org-names-table-actions)))

;;;###autoload
(defun helm-org-names-figure-names (&optional file)
  "Return the names of figures in FILE or the current buffer."
  (with-current-buffer (if file (find-file-noselect file) (current-buffer))
    (org-with-point-at 1
      (let ((names nil))
	(while (re-search-forward org-bracket-link-regexp nil t)
	  (let ((element (org-element-context)))
	    (when (and (eq 'link (org-element-type element))
		       (file-regular-p (org-element-property :path element)))
	      (let* ((name (org-element-property :name (org-element-at-point)))
		     (path (expand-file-name (org-element-property :path element)))
		     (img (create-image path (image-type-from-file-name path) nil :width 120))
		     (thumb (propertize " " 'display img)))
		(when name (push (concat name " :: " thumb) names))))))
	names))))

;;;###autoload
(defun helm-org-names-figure-candidates ()
  (helm-org-names-figure-names helm-org-names-buf))

(setq helm-org-names-figure-actions
      (helm-make-actions
       "Go to figure" (lambda (candidate)
			(let* ((candidate
				(replace-regexp-in-string " ::.+"
							  ""
							  candidate))
			       (case-fold-search t)
			       (point (save-excursion
					(goto-char (point-min))
					(re-search-forward (concat "#\\+name:\s*" candidate) nil t))))
			  (goto-char point)
			  (org-show-entry)))
       "Go to figure (other buffer)" (lambda (candidate)
				       (let ((ind-buf (concat (buffer-name) "--copy")))
					 (when (get-buffer ind-buf)
					   (kill-buffer ind-buf))
					 (clone-indirect-buffer-other-window ind-buf t)
					 (let* ((candidate
						 (replace-regexp-in-string " ::.+"
									   ""
									   candidate))
						(case-fold-search t)
						(point (save-excursion
							 (goto-char (point-min))
							 (re-search-forward (concat "#\\+name:\s*" candidate) nil t))))
					   (goto-char point)
					   (org-show-entry)
					   (recenter 0))))
       "link a figure" (lambda (candidate)
			 (let ((candidate
				(replace-regexp-in-string " ::.+"
							  ""
							  candidate)))
			   (insert (concat "[[" candidate "]]"))))))

(setq helm-org-names-figure-sources
      '((name . "Figures")
	(candidates . helm-org-names-figure-candidates)
	(action . helm-org-names-figure-actions)))

;;;###autoload
(defun helm-org-names ()
  (interactive)
  (setq helm-org-names-buf
	(replace-regexp-in-string
	 "\\(\\.org\\).+" "\\1" (buffer-name)))
  (helm :sources '(helm-org-names-src-sources
		   helm-org-names-table-sources
		   helm-org-names-figure-sources)))

;;; helm-org-names.el ends here
